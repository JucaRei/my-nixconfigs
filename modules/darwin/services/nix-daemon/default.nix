{ lib, config, ... }:

let
  inherit (lib) types mkIf;
  inherit (lib.excalibur) mkOpt enabled;

  cfg = config.excalibur.services.nix-daemon;
in
{
  options.excalibur.services.nix-daemon = {
    enable = mkOpt types.bool true "Whether to enable the Nix daemon.";
  };

  config = mkIf cfg.enable {
    services.nix-daemon = enabled;
  };
}

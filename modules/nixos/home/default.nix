{ options
, config
, pkgs
, lib
, inputs
, ...
}:
with lib;
with lib.excalibur; let
  cfg = config.excalibur.home;
in
{
  options.excalibur.home = with types; {
    file =
      mkOpt attrs { }
        (mdDoc "A set of files to be managed by home-manager's `home.file`.");
    configFile =
      mkOpt attrs { }
        (mdDoc "A set of files to be managed by home-manager's `xdg.configFile`.");
    extraOptions = mkOpt attrs { } "Options to pass directly to home-manager.";
  };

  config = {
    excalibur.home.extraOptions = {
      home.stateVersion = config.system.stateVersion;
      home.file = mkAliasDefinitions options.excalibur.home.file;
      xdg.enable = true;
      xdg.configFile = mkAliasDefinitions options.excalibur.home.configFile;
    };

    snowfallorg.user.${config.excalibur.user.name}.home.config = config.excalibur.home.extraOptions;

    home-manager = {
      useUserPackages = true;
      useGlobalPkgs = true;
    };
  };
}

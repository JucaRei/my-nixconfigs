{ options, config, lib, pkgs, ... }:

with lib;
with lib.excalibur;
let
  cfg = config.excalibur.suites.music;
in
{
  options.excalibur.suites.music = with types; {
    enable = mkBoolOpt false "Whether or not to enable music configuration.";
  };

  config = mkIf cfg.enable {
    excalibur = {
      apps = {
        ardour = enabled;
        bottles = enabled;
      };
    };
  };
}

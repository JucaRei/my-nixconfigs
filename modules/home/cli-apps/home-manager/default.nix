{ lib, config, ... }:

let
  inherit (lib) mkEnableOption mkIf;
  inherit (lib.excalibur) enabled;

  cfg = config.excalibur.cli-apps.home-manager;
in
{
  options.excalibur.cli-apps.home-manager = {
    enable = mkEnableOption "home-manager";
  };

  config = mkIf cfg.enable {
    programs.home-manager = enabled;
  };
}

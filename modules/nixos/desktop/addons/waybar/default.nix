{ options, config, lib, pkgs, ... }:

with lib;
with lib.excalibur;
let cfg = config.excalibur.desktop.addons.waybar;
in
{
  options.excalibur.desktop.addons.waybar = with types; {
    enable =
      mkBoolOpt false "Whether to enable Waybar in the desktop environment.";
  };

  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [ waybar ];

    excalibur.home.configFile."waybar/config".source = ./config;
    excalibur.home.configFile."waybar/style.css".source = ./style.css;
  };
}

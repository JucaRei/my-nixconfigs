{ options, config, lib, pkgs, ... }:

with lib;
with lib.excalibur;
let cfg = config.excalibur.apps.blender;
in
{
  options.excalibur.apps.blender = with types; {
    enable = mkBoolOpt false "Whether or not to enable Blender.";
  };

  config =
    mkIf cfg.enable { environment.systemPackages = with pkgs; [ blender ]; };
}

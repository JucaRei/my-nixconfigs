{ lib, config, pkgs, ... }:

with lib;
with lib.excalibur;
let
  cfg = config.excalibur.apps.expressvpn;
in
{
  options.excalibur.apps.expressvpn = {
    enable = mkEnableOption "Express VPN";
  };

  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [
      excalibur.expressvpn
    ] ++ optionals config.excalibur.desktop.gnome.enable [
      gnomeExtensions.evpn-shell-assistant
    ];

    boot.kernelModules = [ "tun" ];

    systemd.services.expressvpn = {
      wantedBy = [ "multi-user.target" ];
      after = [ "network.target" "network-online.target" ];

      description = "ExpressVPN Daemon";

      serviceConfig = {
        ExecStart = "${pkgs.excalibur.expressvpn}/bin/expressvpnd";
        Restart = "on-failure";
        RestartSec = 5;
      };
    };
  };
}

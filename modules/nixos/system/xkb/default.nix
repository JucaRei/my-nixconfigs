{ options, config, lib, ... }:

with lib;
with lib.excalibur;
let cfg = config.excalibur.system.xkb;
in
{
  options.excalibur.system.xkb = with types; {
    enable = mkBoolOpt false "Whether or not to configure xkb.";
  };

  config = mkIf cfg.enable {
    console.useXkbConfig = true;
    services.xserver = {
      layout = "us";
      xkbOptions = "caps:escape";
    };
  };
}

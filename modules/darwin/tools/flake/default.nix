{ lib, config, pkgs, ... }:

with lib;
with lib.excalibur;
let
  cfg = config.excalibur.tools.flake;
in
{
  options.excalibur.tools.flake = {
    enable = mkEnableOption "Flake";
  };

  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [
      snowfallorg.flake
    ];
  };
}

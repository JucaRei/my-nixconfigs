{ options, config, lib, pkgs, ... }:

with lib;
with lib.excalibur;
let
  cfg = config.excalibur.tools.appimage-run;
in
{
  options.excalibur.tools.appimage-run = with types; {
    enable = mkBoolOpt false "Whether or not to enable appimage-run.";
  };

  config = mkIf cfg.enable {
    excalibur.home.configFile."wgetrc".text = "";

    environment.systemPackages = with pkgs; [
      appimage-run
    ];
  };
}

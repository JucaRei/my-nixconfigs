{ options, config, pkgs, lib, ... }:

with lib;
with lib.excalibur;
let cfg = config.excalibur.system.time;
in
{
  options.excalibur.system.time = with types; {
    enable =
      mkBoolOpt false "Whether or not to configure timezone information.";
  };

  config = mkIf cfg.enable { time.timeZone = "America/Los_Angeles"; };
}

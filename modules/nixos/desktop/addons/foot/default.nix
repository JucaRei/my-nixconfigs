{ options, config, lib, pkgs, ... }:

with lib;
with lib.excalibur;
let cfg = config.excalibur.desktop.addons.foot;
in
{
  options.excalibur.desktop.addons.foot = with types; {
    enable = mkBoolOpt false "Whether to enable the gnome file manager.";
  };

  config = mkIf cfg.enable {
    excalibur.desktop.addons.term = {
      enable = true;
      pkg = pkgs.foot;
    };

    excalibur.home.configFile."foot/foot.ini".source = ./foot.ini;
  };
}

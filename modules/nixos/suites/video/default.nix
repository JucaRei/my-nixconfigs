{ options, config, lib, pkgs, ... }:

with lib;
with lib.excalibur;
let
  cfg = config.excalibur.suites.video;
in
{
  options.excalibur.suites.video = with types; {
    enable = mkBoolOpt false "Whether or not to enable video configuration.";
  };

  config = mkIf cfg.enable {
    excalibur = {
      apps = {
        pitivi = enabled;
        obs = enabled;
      };
    };
  };
}

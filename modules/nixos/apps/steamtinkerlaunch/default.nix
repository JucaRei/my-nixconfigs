{ lib, pkgs, config, ... }:

let
  cfg = config.excalibur.apps.steamtinkerlaunch;

  inherit (lib) mkIf mkEnableOption;
in
{
  options.excalibur.apps.steamtinkerlaunch = {
    enable = mkEnableOption "Steam Tinker Launch";
  };

  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [
      steamtinkerlaunch
    ];
  };
}

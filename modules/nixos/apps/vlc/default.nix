{ options, config, lib, pkgs, ... }:

with lib;
with lib.excalibur;
let
  cfg = config.excalibur.apps.vlc;
in
{
  options.excalibur.apps.vlc = with types; {
    enable = mkBoolOpt false "Whether or not to enable vlc.";
  };

  config = mkIf cfg.enable { environment.systemPackages = with pkgs; [ vlc ]; };
}

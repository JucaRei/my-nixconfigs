{ lib , pkgs, config, osConfig ? { }, format ? "unknown", ...}:
with lib.excalibur; {
  excalibur = {
    cli-apps = {
      zsh = enabled;
      neovim = enabled;
      home-manager = enabled;
    };

    tools = {
      git = enabled;
      direnv = enabled;
    };
  };
}

{ lib
, config
, pkgs
, ...
}:
with lib;
with lib.excalibur; let
  cfg = config.excalibur.cli-apps.tmux;
in
{
  options.excalibur.cli-apps.tmux = {
    enable = mkEnableOption "Tmux";
  };

  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [
      excalibur.tmux
    ];
  };
}

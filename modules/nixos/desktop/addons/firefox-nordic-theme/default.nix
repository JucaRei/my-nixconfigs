{ options, config, lib, pkgs, ... }:

with lib;
with lib.excalibur;
let
  cfg = config.excalibur.desktop.addons.firefox-nordic-theme;
  profileDir = ".mozilla/firefox/${config.excalibur.user.name}";
in
{
  options.excalibur.desktop.addons.firefox-nordic-theme = with types; {
    enable = mkBoolOpt false "Whether to enable the Nordic theme for firefox.";
  };

  config = mkIf cfg.enable {
    excalibur.apps.firefox = {
      extraConfig = builtins.readFile
        "${pkgs.excalibur.firefox-nordic-theme}/configuration/user.js";
      userChrome = ''
        @import "${pkgs.excalibur.firefox-nordic-theme}/userChrome.css";
      '';
    };
  };
}

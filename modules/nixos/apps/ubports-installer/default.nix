{ options, config, lib, pkgs, ... }:

with lib;
with lib.excalibur;
let
  cfg = config.excalibur.apps.ubports-installer;
in
{
  options.excalibur.apps.ubports-installer = with types; {
    enable = mkBoolOpt false "Whether or not to enable the UBPorts Installer.";
  };

  config =
    mkIf cfg.enable {
      environment.systemPackages = with pkgs.excalibur; [
        ubports-installer
      ];

      services.udev.packages = with pkgs.excalibur; [
        ubports-installer-udev-rules
      ];
    };
}

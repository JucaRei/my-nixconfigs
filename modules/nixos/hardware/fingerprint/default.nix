{ options, config, pkgs, lib, ... }:

with lib;
with lib.excalibur;
let cfg = config.excalibur.hardware.fingerprint;
in
{
  options.excalibur.hardware.fingerprint = with types; {
    enable = mkBoolOpt false "Whether or not to enable fingerprint support.";
  };

  config = mkIf cfg.enable { services.fprintd.enable = true; };
}

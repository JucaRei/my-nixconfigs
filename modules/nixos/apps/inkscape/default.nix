{ options, config, lib, pkgs, ... }:

with lib;
with lib.excalibur;
let cfg = config.excalibur.apps.inkscape;
in
{
  options.excalibur.apps.inkscape = with types; {
    enable = mkBoolOpt false "Whether or not to enable Inkscape.";
  };

  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [ inkscape-with-extensions google-fonts ];
  };
}

{ lib, config, pkgs, ... }:

let
  inherit (lib) types mkEnableOption mkIf;
  inherit (lib.excalibur) mkOpt enabled;

  cfg = config.excalibur.tools.git;
  user = config.excalibur.user;
in
{
  options.excalibur.tools.git = {
    enable = mkEnableOption "Git";
    userName = mkOpt types.str user.fullName "The name to configure git with.";
    userEmail = mkOpt types.str user.email "The email to configure git with.";
    signingKey =
      mkOpt types.str "1CA1E1BC" "The key ID to sign commits with.";
    # signByDefault = mkOpt types.bool true "Whether to sign commits by default.";
    signByDefault = mkOpt types.bool false "Whether to sign commits by default.";
  };

  config = mkIf cfg.enable {
    programs.git = {
      enable = true;
      inherit (cfg) userName userEmail;
      lfs = enabled;
      signing = {
        key = cfg.signingKey;
        inherit (cfg) signByDefault;
      };
      extraConfig = {
        init = { defaultBranch = "main"; };
        pull = { rebase = true; };
        push = { autoSetupRemote = true; };
        core = { whitespace = "trailing-space,space-before-tab"; };
        safe = {
          directory = "${user.home}/.ssh-configs/config";
        };
      };
    };
  };
}

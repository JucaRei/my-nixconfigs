{ options, config, lib, pkgs, ... }:

with lib;
with lib.excalibur;
let
  cfg = config.excalibur.suites.art;
in
{
  options.excalibur.suites.art = with types; {
    enable = mkBoolOpt false "Whether or not to enable art configuration.";
  };

  config = mkIf cfg.enable {
    excalibur = {
      apps = {
        gimp = enabled;
        inkscape = enabled;
        blender = enabled;
      };
    };
  };
}

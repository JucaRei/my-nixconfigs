{ options, config, lib, pkgs, ... }:

with lib;
with lib.excalibur;
let cfg = config.excalibur.apps.logseq;
in
{
  options.excalibur.apps.logseq = with types; {
    enable = mkBoolOpt false "Whether or not to enable logseq.";
  };

  config =
    mkIf cfg.enable { environment.systemPackages = with pkgs; [ logseq ]; };
}

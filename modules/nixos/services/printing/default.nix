{ options, config, pkgs, lib, ... }:

with lib;
with lib.excalibur;
let cfg = config.excalibur.services.printing;
in
{
  options.excalibur.services.printing = with types; {
    enable = mkBoolOpt false "Whether or not to configure printing support.";
  };

  config = mkIf cfg.enable { services.printing.enable = true; };
}

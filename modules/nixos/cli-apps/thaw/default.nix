{ config
, lib
, pkgs
, ...
}:
with lib;
with lib.excalibur; let
  cfg = config.excalibur.cli-apps.thaw;
in
{
  options.excalibur.cli-apps.thaw = with types; {
    enable = mkBoolOpt false "Whether or not to enable thaw.";
  };

  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [
      snowfallorg.thaw
    ];
  };
}

{ options, config, lib, pkgs, ... }:

with lib;
with lib.excalibur;
let
  cfg = config.excalibur.suites.common;
in
{
  options.excalibur.suites.common = with types; {
    enable = mkBoolOpt false "Whether or not to enable common configuration.";
  };

  config = mkIf cfg.enable {
    programs.zsh = enabled;

    excalibur = {
      nix = enabled;

      apps = {
        iterm2 = enabled;
      };

      cli-apps = {
        # neovim = enabled;
      };

      tools = {
        git = enabled;
        flake = enabled;
      };

      system = {
        fonts = enabled;
        input = enabled;
        interface = enabled;
      };

      security = {
        # gpg = enabled;
      };
    };
  };
}

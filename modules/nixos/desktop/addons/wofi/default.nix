{ options, config, lib, pkgs, ... }:

with lib;
with lib.excalibur;
let cfg = config.excalibur.desktop.addons.wofi;
in
{
  options.excalibur.desktop.addons.wofi = with types; {
    enable =
      mkBoolOpt false "Whether to enable the Wofi in the desktop environment.";
  };

  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [ wofi wofi-emoji ];

    # config -> .config/wofi/config
    # css -> .config/wofi/style.css
    # colors -> $XDG_CACHE_HOME/wal/colors
    # excalibur.home.configFile."foot/foot.ini".source = ./foot.ini;
    excalibur.home.configFile."wofi/config".source = ./config;
    excalibur.home.configFile."wofi/style.css".source = ./style.css;
  };
}

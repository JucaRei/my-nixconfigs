{ options, config, pkgs, lib, ... }:

with lib;
with lib.excalibur;
let
  cfg = config.excalibur.tools.at;
in
{
  options.excalibur.tools.at = with types; {
    enable = mkBoolOpt false "Whether or not to install at.";
    pkg = mkOpt package pkgs.excalibur.at "The package to install as at.";
  };

  config = mkIf cfg.enable {
    environment.systemPackages = [
      cfg.pkg
    ];
  };
}

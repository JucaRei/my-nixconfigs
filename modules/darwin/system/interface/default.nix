{ options, config, pkgs, lib, ... }:

with lib;
with lib.excalibur;
let cfg = config.excalibur.system.interface;
in
{
  options.excalibur.system.interface = with types; {
    enable = mkEnableOption "macOS interface";
  };

  config = mkIf cfg.enable {
    system.defaults = {
      dock.autohide = true;

      finder = {
        AppleShowAllExtensions = true;
        FXEnableExtensionChangeWarning = false;
      };

      NSGlobalDomain = {
        _HIHideMenuBar = true;
        AppleShowScrollBars = "Always";
      };
    };

    excalibur.home.file.".hushlogin".text = "";
  };
}
